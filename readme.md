#Prime Tester#
Il programma serve per trovare i numeri primi.
Questo crea dei file ___.pt___ (prime tester) che conterranno i numeri trovati.
I file ___dec_num_csv.pt___ e ___bin_num_csv.pt___ avranno, tolto la prima riga, solo i capi per le unità 1 3 7 9.

##Problemi:##
Il fatto che il programma non registri sul file last_num.pt l'ultimo numero rischia, in caso di di mancanza di corrente, di dover rifare il conteggio degli ultimi numeri con conseguenti duplicati.
La soluzione immediata è quella di salvare ogni volta l'ultimo numero esaminato.


