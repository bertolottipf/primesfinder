#!/usr/bin/python3

import sys

#execfile('conf.py')
#execfile('File.py')
#execfile('IsPrime.py')

exec(open("./conf.py").read())
exec(open("./File.py").read())
exec(open("./IsPrime.py").read())
exec(open("./Log.py").read())

		
def getUnits(num):
	n = str(num)
	return int(n[-1:])


class PrimeTester(object):

	"""docstring for PrimeTester"""
	try:
		



		#setto il file dei numeri decimali
		decfile = File(dec_num_file)

		#setto il file dei numeri binari
		binfile = File(bin_num_file)

		#setto il file dei numeri decimali csv
		decfilecsv = File(dec_num_csv_file)

		#setto il file dei numeri binari csv
		binfilecsv = File(bin_num_csv_file)

		#prendo l'ultimo numero che esiste.
		lastnumfile = File(last_num_file)
		try:
			lastnum = int(lastnumfile.read())
		except(IOError, OSError):
			#il file non esiste, oppure non hai i permessi sufficienti
			#lo creo
			lastnumfile.write("1")
			#setto l'ultimo numero a 1
			lastnum = 1

		isPrime = IsPrime()

		go = 0

		#finche go e 0
		while go == 0:


			ip = isPrime.controll(lastnum)
			
			#Log("lastnum = " + str(lastnum))
			#Log("ip = " + str(ip))

			if ip == 0:

				try:
					#scrivo il numero nel file dei decimali
					decfile.append(str(lastnum) + endline)
				except (IOError, OSError):
					#il file non esiste, oppure non hai i permessi sufficienti
					decfile.write(str(lastnum) + endline)


				try:
					#scrivo il numero nel file dei binari
					binfile.append(str("{0:b}".format(lastnum)) + endline)
				except (IOError, OSError):
					#il file non esiste, oppure non hai i permessi sufficienti
					binfile.write(str("{0:b}".format(lastnum)) + endline)

				try:
					#scrivo il numero nel file dei decimali csv
					decfilecsv.append(str(lastnum))
				except (IOError, OSError):
					#il file non esiste, oppure non hai i permessi sufficienti
					decfilecsv.write(str(lastnum))


				try:
					#scrivo il numero nel file dei binari csv
					binfilecsv.append(str("{0:b}".format(lastnum)))
				except (IOError, OSError):
					#il file non esiste, oppure non hai i permessi sufficienti
					binfilecsv.write(str("{0:b}".format(lastnum)))

			
			units = getUnits(lastnum)
			if units != 9:
				decfilecsv.append(csv_separator)
				binfilecsv.append(csv_separator)
			else:
				decfilecsv.append(endline)
				binfilecsv.append(endline)




			if lastnum < 3:
				lastnum += 1
			elif lastnum > 10 and units == 3:
				lastnum += 4
			else:
				lastnum += 2







	except KeyboardInterrupt:
		# quit
		lastnumfile.write(str(lastnum))
		sys.exit()

	

		
