class File(object):

	"""docstring for File"""
	file = ""

	def __init__(self, file):
		self.file = file
		
	# Scrive un file.
	def write(self, string):
		out_file = open(self.file,"w")
		out_file.write(string)
		out_file.close()
		return 0

	# Legge un file.
	def read(self):
		in_file = open(self.file,"r")
		text = in_file.read()
		in_file.close()
		return text

	# Aggiunge qualcosa a un file
	def append(self, string):
		text = self.read()
		self.write(text + string)

	# Legge una linea di un file
	def readLine(self, line):
		c = 0
		line = file.readlines()
		while (line != ''):
			line = file.readline()